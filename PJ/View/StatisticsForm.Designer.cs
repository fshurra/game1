﻿namespace PJ.View
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartStatisticsPeopleCount = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartStatisticsPeopleScore = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxStatistics = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatisticsPeopleCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatisticsPeopleScore)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStatistics)).BeginInit();
            this.SuspendLayout();
            // 
            // chartStatisticsPeopleCount
            // 
            chartArea1.Name = "ChartArea1";
            this.chartStatisticsPeopleCount.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartStatisticsPeopleCount.Legends.Add(legend1);
            this.chartStatisticsPeopleCount.Location = new System.Drawing.Point(52, 278);
            this.chartStatisticsPeopleCount.Name = "chartStatisticsPeopleCount";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartStatisticsPeopleCount.Series.Add(series1);
            this.chartStatisticsPeopleCount.Size = new System.Drawing.Size(293, 179);
            this.chartStatisticsPeopleCount.TabIndex = 0;
            this.chartStatisticsPeopleCount.Text = "chart1";
            // 
            // chartStatisticsPeopleScore
            // 
            chartArea2.Name = "ChartArea1";
            this.chartStatisticsPeopleScore.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartStatisticsPeopleScore.Legends.Add(legend2);
            this.chartStatisticsPeopleScore.Location = new System.Drawing.Point(375, 279);
            this.chartStatisticsPeopleScore.Name = "chartStatisticsPeopleScore";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartStatisticsPeopleScore.Series.Add(series2);
            this.chartStatisticsPeopleScore.Size = new System.Drawing.Size(270, 177);
            this.chartStatisticsPeopleScore.TabIndex = 1;
            this.chartStatisticsPeopleScore.Text = "chart2";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBoxStatistics);
            this.panel1.Location = new System.Drawing.Point(34, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(611, 248);
            this.panel1.TabIndex = 3;
            // 
            // pictureBoxStatistics
            // 
            this.pictureBoxStatistics.Location = new System.Drawing.Point(19, 19);
            this.pictureBoxStatistics.Name = "pictureBoxStatistics";
            this.pictureBoxStatistics.Size = new System.Drawing.Size(581, 218);
            this.pictureBoxStatistics.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxStatistics.TabIndex = 0;
            this.pictureBoxStatistics.TabStop = false;
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 469);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chartStatisticsPeopleScore);
            this.Controls.Add(this.chartStatisticsPeopleCount);
            this.Name = "StatisticsForm";
            this.Text = "StatisticsForm";
            ((System.ComponentModel.ISupportInitialize)(this.chartStatisticsPeopleCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatisticsPeopleScore)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStatistics)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartStatisticsPeopleCount;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartStatisticsPeopleScore;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxStatistics;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PJ.Model;
using PJ.Controller;
namespace PJ.View
{
    public partial class SettingForm : Form
    {
        private MainForm _mainform;
        public SettingForm()
        {
            InitializeComponent();
        }
        public SettingForm(MainForm mainform)
        {
            InitializeComponent();
            _mainform = mainform;
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            GameConfigModel config = new GameConfigModel();
            try
            {
                config.maxplayers = Convert.ToInt32(this.textBoxSetMaxPlayer.Text);
                config.memorytimes = Convert.ToInt32(this.textBoxSetMemoryLImit.Text);
                config.strategy_changing_threshold = Convert.ToInt32(this.textBoxSetThreshold.Text);
                config.chosen_strategies = Convert.ToInt32(this.textBoxSetStrategyLimit.Text);
            }
            catch
            {
                MessageBox.Show("invaild data");
                return;
            }
            _mainform.SetGame(config);
            _mainform.Enabled = true;
            
            this.Close();
        }

        private void buttonSetMemory_Click(object sender, EventArgs e)
        {
            MemorySettingForm msf = new MemorySettingForm();
            msf.ShowDialog();
        }

    }
}

﻿namespace PJ.View
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonAutoSwitch = new System.Windows.Forms.Button();
            this.buttonGoBar = new System.Windows.Forms.Button();
            this.buttonStayHome = new System.Windows.Forms.Button();
            this.buttonSetting = new System.Windows.Forms.Button();
            this.buttonStatistics = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.dataShortMemory = new System.Windows.Forms.DataGridView();
            this.textLog = new System.Windows.Forms.TextBox();
            this.buttonMemorySetting = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataShortMemory)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAutoSwitch
            // 
            this.buttonAutoSwitch.Location = new System.Drawing.Point(9, 12);
            this.buttonAutoSwitch.Name = "buttonAutoSwitch";
            this.buttonAutoSwitch.Size = new System.Drawing.Size(122, 46);
            this.buttonAutoSwitch.TabIndex = 0;
            this.buttonAutoSwitch.Text = "AutoRun";
            this.buttonAutoSwitch.UseVisualStyleBackColor = true;
            this.buttonAutoSwitch.Click += new System.EventHandler(this.buttonAutoSwitch_Click);
            // 
            // buttonGoBar
            // 
            this.buttonGoBar.Location = new System.Drawing.Point(9, 139);
            this.buttonGoBar.Name = "buttonGoBar";
            this.buttonGoBar.Size = new System.Drawing.Size(111, 49);
            this.buttonGoBar.TabIndex = 1;
            this.buttonGoBar.Text = "GoBar";
            this.buttonGoBar.UseVisualStyleBackColor = true;
            this.buttonGoBar.Click += new System.EventHandler(this.buttonGoBar_Click);
            // 
            // buttonStayHome
            // 
            this.buttonStayHome.Location = new System.Drawing.Point(137, 141);
            this.buttonStayHome.Name = "buttonStayHome";
            this.buttonStayHome.Size = new System.Drawing.Size(111, 47);
            this.buttonStayHome.TabIndex = 2;
            this.buttonStayHome.Text = "StayHome";
            this.buttonStayHome.UseVisualStyleBackColor = true;
            this.buttonStayHome.Click += new System.EventHandler(this.buttonStayHome_Click);
            // 
            // buttonSetting
            // 
            this.buttonSetting.Location = new System.Drawing.Point(260, 329);
            this.buttonSetting.Name = "buttonSetting";
            this.buttonSetting.Size = new System.Drawing.Size(116, 47);
            this.buttonSetting.TabIndex = 3;
            this.buttonSetting.Text = "Setting";
            this.buttonSetting.UseVisualStyleBackColor = true;
            this.buttonSetting.Click += new System.EventHandler(this.buttonSetting_Click);
            // 
            // buttonStatistics
            // 
            this.buttonStatistics.Location = new System.Drawing.Point(12, 326);
            this.buttonStatistics.Name = "buttonStatistics";
            this.buttonStatistics.Size = new System.Drawing.Size(119, 50);
            this.buttonStatistics.TabIndex = 4;
            this.buttonStatistics.Text = "Statistics";
            this.buttonStatistics.UseVisualStyleBackColor = true;
            this.buttonStatistics.Click += new System.EventHandler(this.buttonStatistics_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(137, 328);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(117, 46);
            this.buttonReset.TabIndex = 5;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            // 
            // dataShortMemory
            // 
            this.dataShortMemory.AllowUserToAddRows = false;
            this.dataShortMemory.AllowUserToDeleteRows = false;
            this.dataShortMemory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataShortMemory.Location = new System.Drawing.Point(9, 64);
            this.dataShortMemory.Name = "dataShortMemory";
            this.dataShortMemory.ReadOnly = true;
            this.dataShortMemory.Size = new System.Drawing.Size(459, 69);
            this.dataShortMemory.TabIndex = 6;
            // 
            // textLog
            // 
            this.textLog.Location = new System.Drawing.Point(550, 44);
            this.textLog.Multiline = true;
            this.textLog.Name = "textLog";
            this.textLog.Size = new System.Drawing.Size(137, 274);
            this.textLog.TabIndex = 7;
            // 
            // buttonMemorySetting
            // 
            this.buttonMemorySetting.Location = new System.Drawing.Point(385, 333);
            this.buttonMemorySetting.Name = "buttonMemorySetting";
            this.buttonMemorySetting.Size = new System.Drawing.Size(116, 42);
            this.buttonMemorySetting.TabIndex = 8;
            this.buttonMemorySetting.Text = "MemorySetting";
            this.buttonMemorySetting.UseVisualStyleBackColor = true;
            this.buttonMemorySetting.Click += new System.EventHandler(this.buttonMemorySetting_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 388);
            this.Controls.Add(this.buttonMemorySetting);
            this.Controls.Add(this.textLog);
            this.Controls.Add(this.dataShortMemory);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonStatistics);
            this.Controls.Add(this.buttonSetting);
            this.Controls.Add(this.buttonStayHome);
            this.Controls.Add(this.buttonGoBar);
            this.Controls.Add(this.buttonAutoSwitch);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataShortMemory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAutoSwitch;
        private System.Windows.Forms.Button buttonGoBar;
        private System.Windows.Forms.Button buttonStayHome;
        private System.Windows.Forms.Button buttonSetting;
        private System.Windows.Forms.Button buttonStatistics;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.DataGridView dataShortMemory;
        private System.Windows.Forms.TextBox textLog;
        private System.Windows.Forms.Button buttonMemorySetting;
        private System.Windows.Forms.Timer timer1;
    }
}


﻿namespace PJ.View
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonSet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSetMemoryLImit = new System.Windows.Forms.TextBox();
            this.textBoxSetStrategyLimit = new System.Windows.Forms.TextBox();
            this.textBoxSetThreshold = new System.Windows.Forms.TextBox();
            this.buttonSetMemory = new System.Windows.Forms.Button();
            this.textBoxSetMaxPlayer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonSet
            // 
            this.buttonSet.Location = new System.Drawing.Point(269, 113);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(110, 30);
            this.buttonSet.TabIndex = 0;
            this.buttonSet.Text = "Set";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Memory Limit=";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Strategy Limit=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Threshold=";
            // 
            // textBoxSetMemoryLImit
            // 
            this.textBoxSetMemoryLImit.Location = new System.Drawing.Point(171, 46);
            this.textBoxSetMemoryLImit.Name = "textBoxSetMemoryLImit";
            this.textBoxSetMemoryLImit.Size = new System.Drawing.Size(56, 20);
            this.textBoxSetMemoryLImit.TabIndex = 4;
            // 
            // textBoxSetStrategyLimit
            // 
            this.textBoxSetStrategyLimit.Location = new System.Drawing.Point(171, 80);
            this.textBoxSetStrategyLimit.Name = "textBoxSetStrategyLimit";
            this.textBoxSetStrategyLimit.Size = new System.Drawing.Size(56, 20);
            this.textBoxSetStrategyLimit.TabIndex = 5;
            // 
            // textBoxSetThreshold
            // 
            this.textBoxSetThreshold.Location = new System.Drawing.Point(171, 114);
            this.textBoxSetThreshold.Name = "textBoxSetThreshold";
            this.textBoxSetThreshold.Size = new System.Drawing.Size(56, 20);
            this.textBoxSetThreshold.TabIndex = 6;
            // 
            // buttonSetMemory
            // 
            this.buttonSetMemory.Location = new System.Drawing.Point(269, 77);
            this.buttonSetMemory.Name = "buttonSetMemory";
            this.buttonSetMemory.Size = new System.Drawing.Size(110, 30);
            this.buttonSetMemory.TabIndex = 7;
            this.buttonSetMemory.Text = "SetMemory";
            this.buttonSetMemory.UseVisualStyleBackColor = true;
            this.buttonSetMemory.Click += new System.EventHandler(this.buttonSetMemory_Click);
            // 
            // textBoxSetMaxPlayer
            // 
            this.textBoxSetMaxPlayer.Location = new System.Drawing.Point(171, 12);
            this.textBoxSetMaxPlayer.Name = "textBoxSetMaxPlayer";
            this.textBoxSetMaxPlayer.Size = new System.Drawing.Size(56, 20);
            this.textBoxSetMaxPlayer.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(60, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "Max Player=";
            // 
            // SettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 155);
            this.Controls.Add(this.textBoxSetMaxPlayer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonSetMemory);
            this.Controls.Add(this.textBoxSetThreshold);
            this.Controls.Add(this.textBoxSetStrategyLimit);
            this.Controls.Add(this.textBoxSetMemoryLImit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSet);
            this.Name = "SettingForm";
            this.Text = "SettingForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSetMemoryLImit;
        private System.Windows.Forms.TextBox textBoxSetStrategyLimit;
        private System.Windows.Forms.TextBox textBoxSetThreshold;
        private System.Windows.Forms.Button buttonSetMemory;
        private System.Windows.Forms.TextBox textBoxSetMaxPlayer;
        private System.Windows.Forms.Label label4;
    }
}
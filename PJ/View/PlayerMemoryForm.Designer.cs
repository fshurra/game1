﻿namespace PJ.View
{
    partial class PlayerMemoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataPlayerMemory = new System.Windows.Forms.DataGridView();
            this.comboBoxSelectPlayer = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataPlayerMemory)).BeginInit();
            this.SuspendLayout();
            // 
            // dataPlayerMemory
            // 
            this.dataPlayerMemory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataPlayerMemory.Location = new System.Drawing.Point(4, 32);
            this.dataPlayerMemory.Name = "dataPlayerMemory";
            this.dataPlayerMemory.Size = new System.Drawing.Size(644, 341);
            this.dataPlayerMemory.TabIndex = 0;
            // 
            // comboBoxSelectPlayer
            // 
            this.comboBoxSelectPlayer.FormattingEnabled = true;
            this.comboBoxSelectPlayer.Location = new System.Drawing.Point(6, 2);
            this.comboBoxSelectPlayer.Name = "comboBoxSelectPlayer";
            this.comboBoxSelectPlayer.Size = new System.Drawing.Size(247, 21);
            this.comboBoxSelectPlayer.TabIndex = 1;
            // 
            // PlayerMemoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 373);
            this.Controls.Add(this.comboBoxSelectPlayer);
            this.Controls.Add(this.dataPlayerMemory);
            this.Name = "PlayerMemoryForm";
            this.Text = "UserMemoryForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataPlayerMemory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataPlayerMemory;
        private System.Windows.Forms.ComboBox comboBoxSelectPlayer;
    }
}
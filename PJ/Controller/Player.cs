﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJ.Model;
namespace PJ.Controller
{
    public class Player
    {
        protected List<Strategy> _strategies;
        protected Memory _memory;     // Short Term 
        protected int _playerid;      
        protected string _history; // WIN OR LOSE
        protected List<int> _choice; // Home OR Out
        protected int _currentStrategy; // the current strategy
        protected int _changingThreshold;
        protected List<int> _money;

        public Player()
        {

        }


        public bool setInitData(int id, int strategycount, int memorycount,int threshold)
        {
            _strategies = new List<Strategy>();
            initStrategies(strategycount,memorycount);
            //gen memory init for players
            _memory = new Memory(memorycount, genInitMemory(memorycount));
            //
            _playerid = id;
            _history = "";
            _choice = new List<int>();
            _currentStrategy = 0;
            _changingThreshold = threshold;
            _money = new List<int>();
            _money.Add(0);
            return true;

        }

        
        virtual public int doNextRoundStart(int humanChoice)        //modify
        {
            // maybe better called get next choice
            Strategy now = chooseStrategy();
            int choice = now.getResponse(_memory.getMemory());
            // refresh history choice
            _choice.Add(choice);
            return choice;
        }

        public bool doNextRoundEnd(int winningchoice,int bigA)
        {
            // maybe better called refreshing
            // refresh strategy
            foreach (var i in _strategies)
            {
                i.refreshScore(_memory.getMemory(), winningchoice);
            }
            // refresh history result& memory
            int lastchoice = _choice[_choice.Count - 1];
            if (lastchoice != winningchoice)
            {
                _history = _history+"0";
                _memory.setMemory(false);
            }
            else
            {
                _history = _history + "1";
                _memory.setMemory(true);
            }
            //refresh histroy money
            int lastmoney = _money[_money.Count - 1];
            _money.Add(lastmoney + ((-lastchoice) * bigA));
            return true;
        }
        public PlayerInfoModel getPlayerInfo()      //add 2016.6.18 7:26
        {
            Strategy strategyNow = chooseStrategy();
            
            PlayerInfoModel info = new PlayerInfoModel();
            info.choice = new List<int>(_choice);
            info.history = _history;
            info.recommendChoice = strategyNow.getResponse(_memory.getMemory());
            info.currentStrategy = strategyNow;
            info.memory = _memory;
            info.strategies = getStrategyInfo();
            return info;
        }
        public string getMemoryInfo()
        {
            /// this returns a memory string (from old to new)
            return _memory.getMemory();
        }

        public List<int> getChoiceHistroy()
        {
            return _choice;
        }

        public string getResultHistory()
        {
            
            return _history;       
        }

        public List<Strategy> getStrategy()
        {
            /// this is for the Strategy object
            return _strategies;
        }

        public List<string> getStrategyInfo()
        {
            /// this is the strategy in string form
            List<string> tmp = new List<string>();
            foreach (var i in _strategies)
            {
                tmp.Add(i.getSStrategy());      //the sstrategy 2 is -1
            }
            return tmp;
        }

        public int getPlayerID()
        {
            return _playerid;
        }

        protected int initStrategies(int strategycount, int memorycount)
        {
            for (int i = 0; i < strategycount;)
            {
                bool dup = false;
                Strategy n0 = new Strategy(Convert.ToInt32(Math.Pow(2, memorycount)));
                // check if exist duplicate
                string info = n0.getSStrategy();
                for (int j = 0; j < i; j++)
                {
                    if (info == _strategies[j].getSStrategy())
                    {
                        dup = true;
                        break;
                    }
                }
                if (!dup)
                {
                    _strategies.Add(n0);
                    i++;
                }
                
            }
            return 0;
        }

        protected string genInitMemory(int memorycount)
        {
            // Here is the random gen
            string init = "";
            //for (int i = 0; i < memorycount; i++)
            //{
            //    init = init + "0";
            //}
            long tick = DateTime.Now.Ticks;
            Random ra = new Random((int)(tick));
            for (int i = 0; i < memorycount; i++)
            {
                int rand = ra.Next() % 2;
                if (rand == 1)
                {
                    init = init + "1";
                }
                else
                {
                    init = init + "0";
                }
            }
            return init;
        }

        protected Strategy chooseStrategy()
        {
            int currentscore = _strategies[_currentStrategy].Score;
            int counter = 0;
            int newstrategy = -1;
            int maxScore = currentscore;        //modify
            foreach (var i in _strategies)
            {
                if (i.Score>= maxScore)     //modify
                {
                    newstrategy = counter;
                    maxScore = i.Score;
                }
                counter++;
            }
            if (newstrategy != -1 && maxScore - _changingThreshold >= currentscore) //modify
            {
                _currentStrategy = newstrategy;
            }
            return _strategies[_currentStrategy];
        }


        //  public dynamic 
    }

    

    
}

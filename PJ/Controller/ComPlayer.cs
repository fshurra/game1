﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJ.Model;
namespace PJ.Controller
{
    public class ComPlayer : Player
    {
        public ComPlayer(GameConfigModel config, int id)
        {
            this.setInitData(id, config.chosen_strategies, config.memorytimes, config.strategy_changing_threshold);
        }
    }
}

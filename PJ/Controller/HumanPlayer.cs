﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJ.Model;
namespace PJ.Controller
{
    public class HumanPlayer : Player
    {
        private bool _automode;
        public HumanPlayer(GameConfigModel config, int id)
        {
            _automode = false;
            this.setInitData(id, config.chosen_strategies, config.memorytimes, config.strategy_changing_threshold);
        }

        public override int doNextRoundStart(int humanChoice)        //modify
        {
            int choice = humanChoice;
            _choice.Add(choice);
            return choice;
            //if (_automode)
            //{
            //    // maybe better called get next choice
            //    Strategy now = chooseStrategy();
            //    int choice = now.getResponse(_memory.getMemory());
            //    // refresh history choice
            //    _choice.Add(choice);
            //    return choice;
            //}
            //else
            //{
            //    int choice = fetchManualChoice();
            //    _choice.Add(choice);
            //    return choice;
            //}
        }

        public bool startAutomode()
        {
            _automode = true;
            return true;
        }

        public bool stopAutomode()
        {
            _automode = false;
            return true;
        }

        public int getPreferedChoice()
        {
            Strategy now = chooseStrategy();
            int choice = now.getResponse(_memory.getMemory());
            return choice;
        }

    }
}

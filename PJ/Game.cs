﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game1
{
    public class GameConfig
    {

        public int maxrounds;
        public int maxplayers;
        public int memorytimes;
        public int chosen_strategies;
        public int strategy_changing_threshold;
        public int[] probablegameinit;

        public GameConfig()
        {
        }

    }

    public class Game
    {
        // private int _gameid;
        // private int _memorydays;
        private int _maxrounds;
        private int _nowrounds;
        private int _maxplayers;

        private List<Player> _players;
        private List<int> _winningchoices; // the winning choices history
        private List<int> _peopleout;       // the number of people out

        public Game(GameConfig config)
        {
            // _maxrounds = config.
            _maxrounds = config.maxrounds;
            _maxplayers = config.maxplayers;
            _nowrounds = 0;
            _players = new List<Player>();
            _winningchoices = new List<int>();
            _peopleout = new List<int>();

            // addingplayers
            int id = 0;
            for (; id < _maxplayers; id++)
            {
                if (id == 0)
                {
                    addHumanPlayer(config,id);
                }
                else
                {
                    addComPlayer(config, id);
                }
            }

            // and what?
        }

        // Public Methods


        public bool doNextRound()
        {
            /// If this return false. That means it can not do next round. And the simulation comes to the end.
            if (_nowrounds == _maxrounds-1)
            {
                return false;
            }
            int homecount = 0, outcount = 0;
            // geteveryone's choice
            foreach (var player in _players)
            {
                int choice = player.doNextRoundStart();
                if (choice == 1)
                {
                    outcount++;
                }
                else
                {
                    homecount++;
                }
            }
            _peopleout.Add(outcount);
            int winningchoice;
            // getthe winning stat
            if (outcount > homecount)
            {
                winningchoice = 1;
            }
            else
            {
                winningchoice = -1;
            }
            _winningchoices.Add(winningchoice);
            foreach (var player in _players)
            {
                player.doNextRoundEnd(winningchoice);
            }
            _nowrounds++;
            return true;
        }

        public List<Player> getPlayers()
        {
            return _players; 
        }
        public List<int> getPeopleOutHistroy()
        {
            return _peopleout;
        }
        public List<int> getWinningCoiceHistory()
        {
            return _winningchoices;
        }

        // Private Methods
        private bool addHumanPlayer(GameConfig config, int id)
        {
            HumanPlayer hum = new HumanPlayer(config, id);
            _players.Add(hum);
            return true;
        }

        private bool addComPlayer(GameConfig config, int id)
        {
            ComPlayer com = new ComPlayer(config, id);
            _players.Add(com);
            return true;
        }

    }
}

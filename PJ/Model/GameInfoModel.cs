﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJ.Model
{
    public class GameInfoModel
    {
        public List<int> winningChoices { get; set; }
        public int nowRound { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJ.Model
{
    public class GameConfigModel
    {

        public int maxrounds=1000;      //this attribute is not required    //default as 1000
        public int maxplayers;
        public int memorytimes;
        public int chosen_strategies;
        public int strategy_changing_threshold;
        //public int[] probablegameinit;

        public GameConfigModel()
        {
            maxplayers = 0;
            memorytimes = 0;
            chosen_strategies = 0;
            strategy_changing_threshold = 1;
        }
        public GameConfigModel(int maxPlayers,int memoryTimes,int chosenStrategies,int strategyChangingThreshold)
        {
            maxplayers = maxPlayers;
            memorytimes = memoryTimes;
            chosen_strategies=chosenStrategies;
            strategy_changing_threshold = strategyChangingThreshold;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJ.Controller;
namespace PJ.Model
{
    public class PlayerInfoModel
    {
        
        public int recommendChoice{ get; set; }
        public string history{ get; set; }
        public List<int> choice{ get; set; }
        public Strategy currentStrategy{ get; set; }
        public List<String> strategies{ get; set; }
        public Memory memory{ get; set; }


    }
}

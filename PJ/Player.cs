﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game1
{
    public class Player
    {
        protected List<Strategy> _strategies;
        protected Memory _memory;     // Short Term 
        protected int _playerid;      
        protected List<bool> _history; // WIN OR LOSE
        protected List<int> _choice; // Home OR Out
        protected int _currentStrategy; // the current strategy
        protected int _changingThreshold;

        public Player()
        {

        }


        public bool setInitData(int id, int strategycount, int memorycount,int threshold)
        {
            _strategies = new List<Strategy>();
            initStrategies(strategycount,memorycount);
            //gen memory init for players
            _memory = new Memory(memorycount, genInitMemory(memorycount));
            //
            _playerid = id;
            _history = new List<bool>();
            _choice = new List<int>();
            _currentStrategy = 0;
            _changingThreshold = threshold;
            return true;

        }

        
        public int doNextRoundStart()
        {
            // maybe better called get next choice
            Strategy now = chooseStrategy();
            int choice = now.getResponse(_memory.getMemory());
            // refresh history choice
            _choice.Add(choice);
            return choice;
        }

        public bool doNextRoundEnd(int winningchoice)
        {
            // maybe better called refreshing
            // refresh strategy
            foreach (var i in _strategies)
            {
                i.refreshScore(_memory.getMemory(), winningchoice);
            }
            // refresh history result& memory
            int lastchoice = _choice[_choice.Count - 1];
            if (lastchoice != winningchoice)
            {
                _history.Add(false);
                _memory.setMemory(false);
            }
            else
            {
                _history.Add(true);
                _memory.setMemory(false);
            }
            

            return true;
        }

        public string getMemoryInfo()
        {
            /// this returns a memory string (from old to new)
            return _memory.getMemory();
        }

        public List<int> getChoiceHistroy()
        {
            return _choice;
        }

        public List<bool> getResultHistory()
        {
            
            return _history;       
        }

        public List<Strategy> getStrategy()
        {
            /// this is for the Strategy object
            return _strategies;
        }

        public List<string> getStrategyInfo()
        {
            /// this is the strategy in string form
            List<string> tmp = new List<string>();
            foreach (var i in _strategies)
            {
                tmp.Add(i.getSStrategy());      //the sstrategy 2 is -1
            }
            return tmp;
        }

        public int getPlayerID()
        {
            return _playerid;
        }

        protected int initStrategies(int strategycount, int memorycount)
        {
            for (int i = 0; i < strategycount; i++)
            {
                Strategy n0 = new Strategy(Convert.ToInt32(Math.Pow(2, memorycount)));
                // check if exist duplicate
                int info = n0.GetHashCode();
                for (int j = 0; j < i; j++)
                {
                    while (info != _strategies[j].GetHashCode())
                    {
                        n0 = new Strategy(Convert.ToInt32(Math.Pow(2, memorycount)));
                        info = n0.GetHashCode();
                    }
                }
                _strategies.Add(n0);
            }
            return 0;
        }

        protected string genInitMemory(int memorycount)
        {
            string init = "";
            for (int i = 0; i < init.Length; i++)
            {
                init = init + "0";
            }
            return init;
        }

        protected Strategy chooseStrategy()
        {
            int currentscore = _strategies[_currentStrategy].Score;
            int counter = 0;
            int newstrategy = -1;
            foreach (var i in _strategies)
            {
                if (i.Score - _changingThreshold >= currentscore)
                {
                    newstrategy = counter;
                }
                counter++;
            }
            if (newstrategy != -1)
            {
                _currentStrategy = newstrategy;
            }
            return _strategies[_currentStrategy];
        }


        //  public dynamic 
    }

    // Here start the sub class of the Player
    public class HumanPlayer : Player
    {
        private bool _automode;
        public HumanPlayer(GameConfig config, int id)
        {
            _automode = false;
            this.setInitData(id, config.chosen_strategies, config.memorytimes, config.strategy_changing_threshold);
        }

        new public int doNextRoundStart()
        {
            if (_automode)
            {
                // maybe better called get next choice
                Strategy now = chooseStrategy();
                int choice = now.getResponse(_memory.getMemory());
                // refresh history choice
                _choice.Add(choice);
                return choice;
            }
            else
            {
                int choice = fetchManualChoice();
                _choice.Add(choice);
                return choice;
            }
        }

        public bool startAutomode()
        {
            _automode = true;
            return true;
        }

        public bool stopAutomode()
        {
            _automode = false;
            return true;
        }

        public int getPreferedChoice()
        {
            Strategy now = chooseStrategy();
            int choice = now.getResponse(_memory.getMemory());
            return choice;
        }
    
    }

    public class ComPlayer : Player
    {
        public ComPlayer(GameConfig config, int id)
        {
            this.setInitData(id, config.chosen_strategies, config.memorytimes, config.strategy_changing_threshold);
        }
    }
}
